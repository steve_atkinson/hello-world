package uk.org.windswept;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by steveatkinson on 16/03/2016.
 */
public class HelloWorldTest
{

    @Test
    public void shouldHelloWorld () throws Exception
    {
          assertThat (new HelloWorld().helloWorld(), is("Hello World"));
    }
}